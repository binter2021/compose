DETACHED:=-d

project-build: 
	docker-compose build backend-api-task backend-schedulator frontend-app-tasks

project-up: stop-all 
	docker-compose up $(DETACHED)  --remove-orphans  backend-api-task backend-schedulator frontend-app-tasks

project-run: project-up

stop-all: 
	docker-compose stop

project-exec-api-task:
	docker-compose exec  backend-api-task /bin/bash
project-exec-app:
	docker-compose exec  frontend-app-tasks /bin/bash

project-logs:
	docker-compose logs -f