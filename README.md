# Compose para levantar los artefactos necesarios para el proyecto de tareas

## Descripción

Docker compose, encargado de orquestar los artefactos de Front (app-task) Backend (api-task, scheduler)

## Pre Requisitos

1. Tener instalado versión 19.0+ docker más informacion [link](https://docs.docker.com/engine/install/)
2. Tener instalado Docker-Compose 1.29.1 [link](https://docs.docker.com/compose/install/)
2. Contar con git instalado versión 2.20+
3. Tener instalado Makefile 3.0+
4. Clonar  app front desde el siguiente reporsitorio [link](https://gitlab.com/binter2021/app-task)
5. Clonar api task desde el siguiente reporsitorio [link](https://gitlab.com/binter2021/api-task)
6. Clonar sheduler desde el siguiente reporsitorio [link](https://github.com/bi-lriveros/schedulator)
7. Clonar el presente proyecto (docker compose)

## Instalación/Ejecución Local

1.- Configurar las variables de entorno para el pryecto de compose, copiando el archivo de template

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
cp env_template .env
```
2.- Editar el archivo `.env` e indicar la ruta absoluta a cada proyecto clonado en el punto 4,5,6


4.- Ingresar donde se clonó el docker compose y hacer build de los artefactos

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-build
```

5.- Levantar los artefactos, siempre prosicionado en el directorio de proyecto compose 

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-run
```

6.- Ingresar al api-task (backend) y levantar el spring-boot, siempre prosicionado en el directorio de proyecto compose 

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-exec-api-task
$ mvn spring-boot:run
```

7.- Ingresar al app-task (front), instalar dependencias y levantar el react, siempre prosicionado en el directorio de proyecto compose  (la velocidad de descarga dependerá de la conexión y host)

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-exec-app
$ yarn
$ yarn start
```

## Visualización front

1.- Ingresar a su navegador favorito e ingresar http://localhost:3000


## Visualizar logs 
```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-logs

``
